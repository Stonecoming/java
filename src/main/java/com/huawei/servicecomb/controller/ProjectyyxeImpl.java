package com.huawei.servicecomb.controller;


import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.apache.servicecomb.provider.rest.common.RestSchema;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.CseSpringDemoCodegen", date = "2019-10-18T01:17:29.226Z")

@RestSchema(schemaId = "projectyyxe")
@RequestMapping(path = "/rest", produces = MediaType.APPLICATION_JSON)
public class ProjectyyxeImpl {

    @Autowired
    private ProjectyyxeDelegate userProjectyyxeDelegate;


    @RequestMapping(value = "/helloworld",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    public String helloworld( @RequestParam(value = "name", required = true) String name){

        return userProjectyyxeDelegate.helloworld(name);
    }

}
